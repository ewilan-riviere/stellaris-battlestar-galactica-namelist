# Stellaris · Battlestar Galactica Namelist

Stellaris Namelist about battelstar Galactica, host on [**Steam**](https://steamcommunity.com/sharedfiles/filedetails/?id=785079501)

- Windows path: `C:\Users\USER\OneDrive\Documents\Paradox Interactive\Stellaris\mod\battlestar_galactica_namelist`
- Linux path: `~/.local/share/Paradox Interactive/Stellaris/mod/battlestar_galactica_namelist`

## Help

- [**Stellaris Paradox Wiki**](https://stellaris.paradoxwikis.com/Modding)
